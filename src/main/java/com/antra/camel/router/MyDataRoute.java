package com.antra.camel.router;

import org.apache.camel.builder.RouteBuilder;
//import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class MyDataRoute extends RouteBuilder {
	
	/*
	 * @Value("${my.app.source}") private String source;
	 */
	@Override
	public void configure() throws Exception {
        from("file:C:\\Source?noop=true")
        .to("file:C:\\Dest");
        
	}

}
